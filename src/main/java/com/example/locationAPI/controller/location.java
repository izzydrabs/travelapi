package com.example.locationAPI.controller;

import com.example.locationAPI.domain.Geometry;
import com.example.locationAPI.domain.Leg;
import com.example.locationAPI.domain.Step;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.*;
import com.example.locationAPI.domain.Route;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@RestController
public class location {


    // REST TEMPLATE - MAYBE AN AUTOWIRE
    @Bean
    public RestTemplate restTemplateForLocation(RestTemplateBuilder builder) { return builder.build();}

    @GetMapping("/location/{startLat},{startLon}/{endLat},{endLon}")
    public ResponseEntity getLocation(RestTemplate restTemplate, @PathVariable String startLat, @PathVariable String startLon, @PathVariable String endLat, @PathVariable String endLon) throws JsonProcessingException{

        List<Route> routeList = new ArrayList<>();

        String requestURL = "https://api.mapbox.com/directions/v5/mapbox/driving/" + startLat + "," + startLon + ";" + endLat + "," + endLon + "?alternatives=true&geometries=geojson&steps=true&access_token=pk.eyJ1IjoibGV4amFtZXMwNiIsImEiOiJja2ZwYnJ2bW8wMndxMnhvZjY3bnpqeDdlIn0._WIsBZqmlrlS9FA_Vbbr9g";


        String routeResponse = restTemplate.getForObject(requestURL, String.class);

        JSONObject routeJSObject = new JSONObject(routeResponse);

        JSONArray routeJSArray = routeJSObject.getJSONArray("routes");



//        List<Leg> listOfLegs = new ArrayList<>();
//        List<JSONArray> anotherListOfLegs = new ArrayList<>();
//
//
//        for(int i=0; i<routeJSArray.length(); i++){
//
//        List<Leg> legNewOne = new ArrayList<>();
//            // getting the first element of the routes array and calling it a JSON object
//        JSONObject anotherRouteObject = routeJSArray.getJSONObject(i);
//
//        // going inside the leg object in order to map the arrays inside of this
//        JSONArray legArray = anotherRouteObject.getJSONArray("legs");
//
//        anotherListOfLegs.add(legArray);
//
//        // this mapping the leg array to the leg domain
//        legNewOne = new ObjectMapper().readValue(legArray.toString(), new TypeReference<List<Leg>>() {
//        });
//
//
//        Stream.of(legNewOne)
//                .forEach(listOfLegs::addAll);
//
//
//
//       }
//
//        // List of step JSON Arrays to map geometry objects later
//        List<JSONArray> stepList = new ArrayList<>();
//        List<Step> anotherListOfSteps = new ArrayList<>();
//
//        // loop through list of leg JSON Arrays
//        for (JSONArray legJSONArray : anotherListOfLegs) {
//
//            // Loop through each element in each Leg JSON Array
//            for(int i=0; i<legJSONArray.length(); i++){
//
//                //
//                List<Step> stepNewOne = new ArrayList<>();
//                // getting the first element of the routes array and calling it a JSON object
//                JSONObject anotherLegObject = legJSONArray.getJSONObject(i);
//
//                // going inside the leg object in order to map the arrays inside of this
//                JSONArray stepArray = anotherLegObject.getJSONArray("steps");
//
//                stepList.add(stepArray);
//
//                // this mapping the leg array to the leg domain
//                stepNewOne = new ObjectMapper().readValue(stepArray.toString(), new TypeReference<List<Step>>() {
//                });
//
//                Stream.of(stepNewOne)
//                        .forEach(anotherListOfSteps::addAll);
//
//            }
//        }
//
//
//
//
//
//
//       // System.out.println(leg);

      //  System.out.println(routeJSArray);


        routeList = new ObjectMapper().readValue(routeJSArray.toString(), new TypeReference<List<Route>>() {
        });



        return ResponseEntity.ok().body(routeList);










    }

    // Get mapping - controller logic




}
