package com.example.locationAPI.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Geometry {



    List<Coordinate> coordinates = new ArrayList<>();


    public Geometry() {
    }


    @Override
    public String toString() {
        return "Geometry{" +
                "coordinates=" + coordinates +
                '}';
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }
}
