package com.example.locationAPI.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.HashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Coordinate {

    HashMap<String, String> coordinateMap = new HashMap<>();

    public Coordinate() {
    }

    public HashMap<String, String> getCoordinateMap() {
        return coordinateMap;
    }

    public void setCoordinateMap(HashMap<String, String> coordinateMap) {
        this.coordinateMap = coordinateMap;
    }
}
