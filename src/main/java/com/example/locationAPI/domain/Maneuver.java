package com.example.locationAPI.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Maneuver {

    String instruction;


    public Maneuver() {
    }

    @Override
    public String toString() {
        return "Maneuver{" +
                "instruction='" + instruction + '\'' +
                '}';
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
}
