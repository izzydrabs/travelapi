package com.example.locationAPI.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Route {

    double duration;
    double distance;
    List<Leg> legs;

    public Route() {
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }



    @Override
    public String toString() {
        return "Route{" +
                "duration=" + duration +
                ", distance=" + distance +
                ", legs=" + legs +
                '}';
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }


}
