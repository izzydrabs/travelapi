package com.example.locationAPI.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Leg {


    List<Step> steps;




    public Leg() {
    }


    @Override
    public String toString() {
        return "Leg{" +
                "steps=" + steps +
                '}';
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }
}
